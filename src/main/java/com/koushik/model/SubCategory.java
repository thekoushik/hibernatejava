package com.koushik.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="subcategory")
public class SubCategory {
	private long id;
	private String name;
	private Category category;
	private Set<SubSubCategory> subSubCategories=new HashSet<SubSubCategory>(); 
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Column(nullable=false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@ManyToOne
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	@OneToMany(cascade=CascadeType.ALL,mappedBy="subCategory")
	public Set<SubSubCategory> getSubSubCategories() {
		return subSubCategories;
	}
	public void setSubSubCategories(Set<SubSubCategory> subSubCategories) {
		this.subSubCategories = subSubCategories;
	}
	public SubCategory() {}
	public SubCategory(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "SubCategory [id=" + id + ", name=" + name + "]";
	}
	
}
