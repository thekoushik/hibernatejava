package com.koushik.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "category")
public class Category{
    private long id;
    private String name;
    
    private Set<SubCategory> subCategories=new HashSet<SubCategory>();
    
    public Category(){}
    public Category(String name){
    	this.name=name;
    }
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public long getId(){
        return this.id;
    }
    public void setId(long id){
        this.id=id;
    }
    @Column(unique=true,nullable=false)
    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name=name;
    }
    @OneToMany(cascade=CascadeType.ALL,mappedBy="category")
	public Set<SubCategory> getSubCategories() {
		return subCategories;
	}
	public void setSubCategories(Set<SubCategory> subCategories) {
		this.subCategories = subCategories;
	}
	@Override
	public String toString() {
		return "Category [id=" + id + ", name=" + name + "]";
	}
    
}