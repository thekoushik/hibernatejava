package com.koushik;

import java.util.Iterator;
import java.util.Set;

import org.hibernate.Session;

import com.koushik.persistence.HibernateUtil;
import com.koushik.model.Category;
import com.koushik.model.Stock;
import com.koushik.model.SubCategory;
import com.koushik.model.SubSubCategory;

public class App {
	Session session;
	public App(){
		session = HibernateUtil.getSessionFactory().openSession();
	}
	
	public boolean Task1(){
		session.beginTransaction();
        Stock stock = new Stock();
        stock.setStockCode("7412");
        stock.setStockName("ABCD");
        session.save(stock);
        session.getTransaction().commit();
        return true;
	}
	public boolean Task2(){
		session.beginTransaction();
		Category c1=new Category("Cat1");
		SubCategory s1=new SubCategory("SubCat1");
		s1.setCategory(c1);
		c1.getSubCategories().add(s1);
		session.saveOrUpdate(c1);
		session.getTransaction().commit();
		return true;
	}
	
	public boolean Task3(){
		session.beginTransaction();
		Category c1=(Category)session.get(Category.class, 1L);
		if(c1==null)
			return false;
		SubCategory s1=new SubCategory("SubCat2");
		s1.setCategory(c1);
		c1.getSubCategories().add(s1);
		session.saveOrUpdate(c1);
		session.getTransaction().commit();
		return true;
	}
	public boolean Task4(){
		Category c1=(Category)session.get(Category.class, 1L);
		if(c1==null)
			return false;
		Iterator<SubCategory> it=c1.getSubCategories().iterator();
		System.out.println("For "+c1);
		while(it.hasNext())
			System.out.println(it.next());
		return true;
	}
	public boolean Task5(){
		session.beginTransaction();
		Category c1=(Category)session.get(Category.class, 1L);
		if(c1==null)
			return false;
		if(c1.getSubCategories().size()==0)
			return false;
		SubCategory s1=null;
		Iterator<SubCategory> i=c1.getSubCategories().iterator();
		while(i.hasNext())
			if((s1=i.next()).getName().equals("SubCat1"))
				break;
		if(s1==null)
			return false;
		SubSubCategory ss1=new SubSubCategory("SubSubCat3");
		ss1.setSubCategory(s1);
		
		s1.getSubSubCategories().add(ss1);
		
		SubSubCategory ss2=new SubSubCategory("SubSubCat4");
		ss2.setSubCategory(s1);
		
		s1.getSubSubCategories().add(ss2);
		
		session.saveOrUpdate(c1);
		session.getTransaction().commit();
		return true;
	}
	public static void main( String[] args ){
        System.out.println("Maven + Hibernate + MySQL");
        App app=new App();
        
        System.out.println( app.Task5() ? "Success" : "Failure");
        
        //HibernateUtil.shutdown();
    }
    
}
